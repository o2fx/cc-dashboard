import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import router from './router'

Vue.use(Vuex);

export default new Vuex.Store({
 state:{
     idToken:localStorage.getItem('token') || null,
     user:localStorage.getItem('user') || null,
 },
 mutations:{
    authUser(state, token,user){
        state.idToken = token;
        state.user = user;
    },
    clearAuthData(state){
        state.idToken = null;
        state.user = null;
    }
 },
 actions:{
    singIn({commit}, authData){
        const url = axios.defaults.baseURL + "/login";
        axios.post(url,{
            user:authData.user,
            passwd:authData.passwd,
        })
        .then(res=>{
            localStorage.setItem('token',res.data.token);
            localStorage.setItem('user',authData.user);
            //axios.defaults.headers.common['Authorization'] = res.data.token
            commit('authUser',{
                token : res.data.token,
                user:authData.user
            })
            router.replace('/Cours');
        })
        .catch(error => console.log(error));
    },
    tryAutoLogin({commit}){
        const tokenLs = localStorage.getItem('token');
        if(!tokenLs){
            return
        }
        const userLs = localStorage.getItem('user');
        commit('authUser',{
            idToken:tokenLs,
            user:userLs
        })
    },
    logout({commit}){
        commit('clearAuthData');
        localStorage.removeItem('token');
        localStorage.removeItem('user'); 
        //delete axios.defaults.headers.common['Authorization'];
        router.replace('/');
    }
 },
getters:{
    user (state){
        return state.user
    },
    isAuth(state){
        return state.idToken !== null
    }
}

});