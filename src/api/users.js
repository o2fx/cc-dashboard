/*jshint esversion : 6 */
//On définit ici les verbes RestFul pour finaliser l'api & on sépare les modèles qui sont dans models

module.exports = (app) => {
    const userModel = require('./models/users');
    const auth = require("./../services/auth");
    const jwt = require('jsonwebtoken');

    const JWT_SIGN_SECRET = '0shdhd1gfhf§ehfjuirjjftyyui123kaegdhdjdrcc';

    const users = () => {

        app.get('/users', (req, res) => {
            userModel.get((response) => {
                res.status(200).send(response);
            })
        });

        app.get('/users/:id', (req, res) => {
            userModel.get((response) => {
              res.send(response);
            }, req.params.id); // le second param est ici !!!
          });

        app.patch('/users/:id',(req,res) => {
            userModel.patch((data) => {
                res.send(data);
            }, req.body, req.params.id)
        });

        app.post('/login', (req, res)=>{
            if(!req.body){
                return res.status(500).send({message : "Accès non authorisé"});
            }
            //params
            let reqUser = req.body.user;
            let reqPasswd = req.body.passwd
            
            userModel.login(response => {
                if(!response.error){
                    const userData={
                        user : response.user,
                        passwd:response.passwd
                    };
                    if(reqUser === userData.user && reqPasswd === userData.passwd){
                        jwt.sign({userData}, JWT_SIGN_SECRET, { expiresIn: '1h' },(err, token) =>{
                            res.json({
                                token,
                            })
                        });
                    } else {
                        res.status(500).send({message : "Accès non authorisé"});
                    }
                }
            });

        });

        


    };

    return users();
}