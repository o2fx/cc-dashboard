const mysql = require("mysql");

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'ccampus',
    password: 'campus1234',
    database: 'elearning',
    socketPath: '/Applications/MAMP/tmp/mysql/mysql.sock',
  })

connection.connect();

// dummy function pour vérifier si la connection à la base est bien établie
connection.query('SELECT 42 AS solution', function (error, results, fields) {
    if (error) throw error;
    console.log('DATABASE SAYS => The solution is', results[0].solution);
  });

const get = (clbk, id) => {
    let query;
  
    if (id) query = `SELECT * FROM contents WHERE id = ${connection.escape(id)}`;
    else query = 'SELECT * FROM contents ORDER BY id';
    
    connection.query(query, (error, results, fields) => {
        if (error) throw error; // en cas d'erreur, une exception est levée
         clbk(results); // on passe les résultats de la requête en argument de la fonction callback
        });
}; 

const register = (clbk, data) => {
    let query = `INSERT INTO contents (title, description, content,img_resume_url, img_thumb_url, categorie_id) VALUES 
    (${connection.escape(data.title)},
    ${connection.escape(data.description)},
    ${connection.escape(data.content)},
    ${connection.escape(data.img_resume_url)},
    ${connection.escape(data.img_thumb_url)},
    ${connection.escape(data.categorie_id)})`;

    connection.query(query, (error, results, fields) => {
        if (error) throw error;
        results.error = false;
        results.message = "Cours enregistré !!!";
        clbk(results);
    });
}


const patch = (clbk, data, id) => {
    let query = `
    UPDATE contents SET
            title = ${connection.escape(data.title)}, 
            description = ${connection.escape(data.description)}, 
            content = ${connection.escape(data.content)} 
            WHERE id = ${connection.escape(id)}
    `;

    connection.query(query, (error, results, fields) => {
        if (error) throw error;
        results.message = "Le cours a été modifié !";
        console.log(results);
        clbk(results);
    })
}
  
const remove = (clbk, data, id) =>{
    let query = `DELETE FROM contents WHERE id = ${connection.escape(id)}`;
    
    connection.query(query, (error, results, fields) => {
        if (error) throw error;
        clbk(results)
    })
}

module.exports = {
    get,
    register,
    patch,
    remove
};