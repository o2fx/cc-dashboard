const mysql = require("mysql");

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'ccampus',
    password: 'campus1234',
    database: 'elearning',
    socketPath: '/Applications/MAMP/tmp/mysql/mysql.sock',
  })

connection.connect();

// dummy function pour vérifier si la connection à la base est bien établie
connection.query('SELECT 42 AS solution', function (error, results, fields) {
    if (error) throw error;
    console.log('DATABASE SAYS => The solution is', results[0].solution);
  });

const get = (clbk, id) => {
    let query;
  
    if (id) query = `SELECT * FROM users WHERE id = ${connection.escape(id)}`;
    else query = 'SELECT * FROM users ORDER BY id';
    
    connection.query(query, (error, results, fields) => {
        if (error) throw error; // en cas d'erreur, une exception est levée
         clbk(results); // on passe les résultats de la requête en argument de la fonction callback
        });
}; 

const patch = (clbk, data, id) => {
    let query = `
    UPDATE users SET
            user = ${connection.escape(data.user)},
            passwd = ${connection.escape(data.passwd)},
            WHERE id = ${connection.escape(id)}
    `;

    connection.query(query, (error, results, fields) => {
        if (error) throw error;
        results.message = "L'utilisateur a été modifié !";
        console.log(results);
        clbk(results);
    })
};

const login = (clbk, data) => {
    // const q = `SELECT id, user, passwd FROM users WHERE user = '${data.user}' AND passwd = '${data.password}' GROUP BY id`;
    const q = `SELECT id, user, passwd FROM users where user='admin'`;
    connection.query(q, (error, results, fields) => {
  
      if (error) throw error;
      const tmp = results[0] || results;
      const res = {};
    
      if (Array.isArray(tmp) && !tmp.length) {
        res.message = "Mauvais utilisateur ou mot de passe";
      } else {
        res.user = tmp;
        res.message = "Vous êtes connecté";
      }

      clbk(res.user);
    }); 
};
  

module.exports = {
    get,
    patch,
    login
};