const mysql = require("mysql");

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'ccampus',
    password: 'campus1234',
    database: 'elearning',
    socketPath: '/Applications/MAMP/tmp/mysql/mysql.sock',
  })

connection.connect();

// dummy function pour vérifier si la connection à la base est bien établie
connection.query('SELECT 42 AS solution', function (error, results, fields) {
    if (error) throw error;
    console.log('DATABASE SAYS => The solution is', results[0].solution);
  });

const get = (clbk, id) => {
    let query;
  
    if (id) query = `SELECT * FROM categories WHERE id = ${connection.escape(id)} ORDER BY id`;
    else query = 'SELECT * FROM categories ORDER BY id';
    
    connection.query(query, (error, results, fields) => {
        if (error) throw error; // en cas d'erreur, une exception est levée
         clbk(results); // on passe les résultats de la requête en argument de la fonction callback
        });
};

const register = (clbk, data) => {
    let query = `INSERT INTO categories (name) VALUES (${connection.escape(data.name)})`;

    connection.query(query, (error, results, fields) => {
        if (error) throw error;
        results.error = false;
        results.message = "Catégorie enregistrée !!!";
        clbk(results);
    });
}

const patch = (clbk, data, id) => {
    let query = `
    UPDATE categories SET
            name = ${connection.escape(data.name)}
            WHERE id = ${connection.escape(id)}
    `;

    connection.query(query, (error, results, fields) => {
        if (error) throw error;
        results.message = "La catégories a été modifiée !";
        clbk(results);
    })
}
  
const remove = (clbk, data, id) =>{
    let query = `DELETE FROM categories WHERE id = ${connection.escape(id)}`;
    
    connection.query(query, (error, results, fields) => {
        if (error) throw error;
        clbk(results)
    })
}

module.exports = {
    get,
    register,
    patch,
    remove
};