//Imports
const express = require("express");
const port = process.env.port || 3000;
const bodyParser = require("body-parser");

//Instanciation du server
const app = express();

app.use(bodyParser.json()); // JSON accepté @param requêtes http
app.use(bodyParser.urlencoded({ extended: true })); // form-urleconded accepté @param requêtes http

//C.O.R.S
app.use((req, res, next)=>{
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Request-With, Content-Type, Accept, Authorization"
    );
    if (req.method === "OPTIONS"){
        res.header("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE, PUT");
        return res.status(200).json({});
    }
    next();
});

//routes
app.get('/', (req, res) => {
    res.status(200).send("node-backend says : Hello World !");
});

//Le reste de l'api
const lessons    = require("./lessons")(app); 
const categories = require("./categories")(app); 
const users = require("./users")(app); 

//Lancement du serveurs
app.listen(port);
