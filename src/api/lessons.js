/*jshint esversion : 6 */
//On définit ici les verbes RestFul pour finaliser l'api & on sépare les modèles qui sont dans models

module.exports = (app) => {
    const lessonModel = require('./models/lessons');
    const multer = require('multer');
    
    const storage = multer.diskStorage({
        destination : 'static/',
        filename: (req, file, clbk) =>{
            clbk(null, file.originalname)
        }
    });

    const upload = multer({storage : storage});

    const lessons = () => {

        app.get('/lessons', (req, res) => {
            lessonModel.get((response) => {
                res.status(200).send(response);
            })
        });

        app.get('/lessons/:id', (req, res) => {
            lessonModel.get((response) => {
              res.send(response);
            }, req.params.id); // le second param est ici !!!
          });

        app.post('/lessons',(req, res) =>{
            try {       
                lessonModel.register(response => {
                    res.status(201).send("Cours créé avec succès");
                }, req.body);
            } catch(err) {
            res.status(500).send("soucis durant la création du cours");
            }
        });

        app.patch('/lessons/:id',(req,res) => {
            lessonModel.patch((data) => {
                res.send(data);
            }, req.body, req.params.id)
        });

        app.delete('/lessons/:id',(req,res)=>{
            lessonModel.remove((data) =>{
                res.send(data)
            }, req.body, req.params.id)
        });

        app.post('/uploads',upload.single('thumb'),(req, res) =>{ //thumb le nom du champ input
            try{
                console.log("REQ 1:",req.file);
            } catch(err){
                res.status(500).send("soucis durant l'upload du fichier"); 
            }
        });
    };

    return lessons();
}