/*jshint esversion : 6 */

//On définit ici les verbes RestFul pour finaliser l'api & on sépare les modèles qui sont dans models

module.exports = (app) => {
    const categorieModel = require('./models/categories');

    const categories = () => {

        app.get('/categories', (req, res) => {
            categorieModel.get((response) => {
                res.status(200).send(response);
            })
        });

        app.get('/categories/:id', (req, res) => {
            categorieModel.get((response) => {
              res.send(response);
            }, req.params.id); // le second param est ici !!!
          });

        app.post('/categories',(req, res) =>{
            try {
                categorieModel.register(response => {
                    res.status(201).send("Catégorie créée avec succès");
                }, req.body);
            } catch(err) {
            res.status(500).send("soucis durant la création de la catégorie");
            }
        });

        app.patch('/categories/:id',(req,res) => {
            categorieModel.patch((data) => {
                res.send(data);
            }, req.body, req.params.id)
        });

        app.delete('/categories/:id',(req,res)=>{
            categorieModel.remove((data) =>{
                res.send(data)
            }, req.body, req.params.id)
        });

    };

    return categories();
}