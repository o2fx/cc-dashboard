import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import router from './router';
import store from './store';


axios.defaults.baseURL = 'http://localhost:3000';


// const token = localStorage.getItem('token')
// if (token) {
//   axios.defaults.headers.common['Authorization'] = token
// }

// const router = new VueRouter({
//   mode: 'history',
//   routes,
// })

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
