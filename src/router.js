/* jshint esversion:6 */

import Vue from 'vue'
import VueRouter from 'vue-router'

import store from './store';

import Cours from "./components/cours/Cours.vue";
import Categories from "./components/categories/Categories.vue";
import Login from './components/login/Login.vue';
import SingleCourse from './components/singlecourse/Singlecourse.vue';
import AddCourse from './components/addcourse/AddCourse.vue';
import Parameters from './components/parameters/Parameters.vue';

Vue.use(VueRouter)

const routes = [
    {
        path:"/",
        name:"login",
        component: Login
    },
    {
        path:"/Cours",
        name:"cours",
        component: Cours,
        beforeEnter: (to, from, next) => {
            if(store.state.idToken){
                next() 
            }else{
                next('/')
            }
        }
    },
    {
        path:"/Categories",
        name:"categories",
        component: Categories,
        beforeEnter: (to, from, next) => {
            if(store.state.idToken){
                next() 
            }else{
                next('/')
            }
        }
    },
    {
        path:"/Singlecourse/:id",
        name:"singlecourse",
        component: SingleCourse,
        beforeEnter: (to, from, next) => {
            if(store.state.idToken){
                next() 
            }else{
                next('/')
            }
        }

    },
    {
        path:"/Addcourse",
        name:"addcourse",
        component: AddCourse,
        beforeEnter: (to, from, next) => {
            if(store.state.idToken){
                next() 
            }else{
                next('/')
            }
        }

    },
    {
        path:"/Parameters",
        name:"parametres",
        component: Parameters,
        beforeEnter: (to, from, next) => {
            if(store.state.idToken){
                next() 
            }else{
                next('/')
            }
        }
    }
]

export default new VueRouter({mode: 'history', routes})